# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=py3-imageio
pkgver=2.30.0
pkgrel=0
pkgdesc="Python library that provides an easy interface to read and write a wide range of image data"
url="https://github.com/imageio/imageio"
license="BSD-2-Clause"
# ppc64le: test failures
# s390x: freeimage
arch="noarch !ppc64le !s390x"
depends="python3 py3-numpy py3-pillow freeimage"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-psutil py3-imageio-ffmpeg py3-fsspec"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/i/imageio/imageio-$pkgver.tar.gz"
builddir="$srcdir/imageio-$pkgver"
options="!check" # intentionally fail without internet(?), todo

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD"/build/lib IMAGEIO_NO_INTERNET=1 pytest -v
}

package() {
	python3 setup.py install --root="$pkgdir"

	# remove unneeded binaries
	# shellcheck disable=2115
	rm -r "$pkgdir"/usr/bin
}

sha512sums="
b069c4fa2ef30984375bb8f23ba47ba3ab1a79991054aae33eabb1c2d98883a34fd9ba5cf3337109f96f235e873b67d6cfa37bd5074986c1b829329dc097ad93  py3-imageio-2.30.0.tar.gz
"

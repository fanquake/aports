# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin-plugins
pkgver=23.04.1
pkgrel=0
pkgdesc="Extra Dolphin plugins"
url="https://www.kde.org/applications/system/dolphin/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
makedepends="
	dolphin-dev
	extra-cmake-modules
	ki18n-dev
	kio-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_svn=OFF # Broken "error: expected unqualified-id before '(' token"
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
576abc54f29f06aa2ab0d034161f159302ad33e794a8118b566e940fdea204ed3ca00e71c5dce8dfc23e876370d77e07221feaf67aea1c84ee3e089ab45a46cc  dolphin-plugins-23.04.1.tar.xz
"

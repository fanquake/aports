# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=networkmanager-qt
pkgver=5.106.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# The excluded tests currently fail
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(manager|settings|activeconnection)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
72f6365a9864a5af3b2e418d5fba84149503c2231ad7a1ac5945b9957d975a7b95581c9c445aac72084b5d5a26175dd61d885cb4907c0acac1506921e0be9998  networkmanager-qt-5.106.0.tar.xz
"

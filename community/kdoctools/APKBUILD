# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdoctools
pkgver=5.106.0
pkgrel=0
pkgdesc="Documentation generation from docbook"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="
	docbook-xml
	docbook-xsl
	"
depends_dev="
	karchive-dev
	ki18n-dev
	libxml2-dev
	libxml2-utils
	libxslt-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	perl-uri
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdoctools-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a0eba0dc36f65bc3e288e91127684aa84e0ea45f600611ffd4abd9f359b1fddb9a907b8d91125e8c4fe71c42a0568ea5415bbffea1452679b61ccdd4df9412c3  kdoctools-5.106.0.tar.xz
"

# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=ginkgo
pkgver=2.9.7
pkgrel=0
pkgdesc="Modern Testing Framework for Go"
url="https://onsi.github.io/ginkgo/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/onsi/ginkgo/archive/v$pkgver/ginkgo-$pkgver.tar.gz
	tests.patch
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o bin/ginkgo ./ginkgo
}

check() {
	# integration tests are slow
	# CodeLocation tests access local files inside project root, so they don't work with -trimpath
	GOFLAGS="${GOFLAGS/-trimpath/}" \
		./bin/ginkgo -r --randomize-all --randomize-suites \
		--skip-package ./integration
}

package() {
	install -Dm755 bin/ginkgo -t "$pkgdir"/usr/bin/
}

sha512sums="
2855c9f4a01fa1c6506e84820a37332abc625c65516e1966d12914990aa5ffe915d43c68057d1251dc1e0e5b848daf41d48c8b62ab1cb4a04d645b2f107c26ab  ginkgo-2.9.7.tar.gz
2fe515438583a51dbd92912a761063882788b3d001d37073986d81b9d4076e0381ee1981623cbf603fb70b59efb60bf941d577ecb8d5af97027d1877c7e164cc  tests.patch
"
